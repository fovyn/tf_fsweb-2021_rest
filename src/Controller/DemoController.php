<?php

namespace App\Controller;

use App\Entity\Demo;
use App\Repository\DemoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DemoController extends AbstractController
{
    /**
     * @Route(path="/", name="demo_index", methods={"GET"})
     * @return JsonResponse
     */
    public function index(DemoRepository $repository): Response
    {
        $demos = $repository->findAllByUsername("Flavian");
        return $this->json($demos, Response::HTTP_OK);
    }

    /**
     * @Route(path="/create", name="demo_create", methods={"POST"}, requirements={})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request, EntityManagerInterface $em): Response {
        $data = json_decode($request->getContent(), true);

        $demo = new Demo();
        $demo->setUsername($data["username"]);
        $demo->setPassword($data["password"]);

        dump($demo);

        $em->persist($demo);
        $em->flush();

        return $this->json($demo);
    }
}
