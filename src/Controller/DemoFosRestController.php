<?php

namespace App\Controller;


use App\Model\DemoCreateDTO;
use App\Repository\DemoRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DemoFosRestController
 * @package App\Controller
 * @Route(path="/api/demo")
 */
class DemoFosRestController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/", name="api_demo_readAll")
     * @Rest\View()
     * @param DemoRepository $repository
     * @return View
     */
    public function readAllAction(DemoRepository $repository) {
        return $this->view([
            "demos" => $repository->findAll(),
            "page" => 1,
            "total" => 42
        ]);
    }

    /**
     * @Rest\Post(path="/", name="api_demo_create")
     * @Rest\View()
     * @ParamConverter("dto", converter="fos_rest.request_body")
     * @param EntityManagerInterface $em
     * @return View
     */
    public function createAction(EntityManagerInterface $em, ConstraintViolationList $violations, DemoCreateDTO $dto) {

        if (sizeof($violations) > 0) {
            return $this->view(["errors" => $violations]);
        }

        $demo = $dto->toEntity();
        $em->persist($demo);
        $em->flush();

        return $this->view(["demo" => ""], Response::HTTP_CREATED);
    }
}
