<?php


namespace App\Model;


use App\Entity\Demo;
use Symfony\Component\Validator\Constraints as Assert;

class DemoCreateDTO
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=5, minMessage="Le username doit être de plus de 5 caractères")
     */
    private string $username;
    /**
     * @var string
     * @Assert\NotBlank(message="Le champs password est obligatoire")
     */
    private string $password;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return DemoCreateDTO
     */
    public function setUsername(string $username): DemoCreateDTO
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return DemoCreateDTO
     */
    public function setPassword(string $password): DemoCreateDTO
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return Demo
     */
    public function toEntity(): Demo {
        $demo = new Demo();

        $demo
            ->setUsername($this->username)
            ->setPassword($this->password);

        return $demo;
    }
}